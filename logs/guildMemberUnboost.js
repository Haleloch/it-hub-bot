const discord = require('discord.js');

module.exports = async (client, member) => {
  let logs = client.channels.cache.get(client.settings.logsChannelId);
  if (!logs) return;

  let embed = new Discord.MessageEmbed()
    .setColor('#1a356f')
    .setDescription(`
      **\`${member.user.tag}\` a arrêté de booster le serveur.**`)
    .setThumbnail(member.user.displayAvatarURL({}))
  .setTimestamp();

  logs.send(embed)
}
