const Discord = require('discord.js');

module.exports = async (client, channel) => {
  let logs = client.channels.cache.get(client.settings.logsChannelId);
  if (!logs) return;

  if (!channel.guild) return;

  let embed = new Discord.MessageEmbed()
    .setColor('#1a356f')
    .setDescription(`
      **Salon créé :**

      Nom du salon : \`${channel.name}\`
      Type de salon : \`${channel.type}\``)
    .setTimestamp();

    logs.send(embed)
}
