const discord = require('discord.js');

module.exports = async (client, member, oldNick, newNick) => {
  let logs = client.channels.cache.get(client.settings.logsChannelId);
  if (!logs) return;

  let embed = new Discord.MessageEmbed()
    .setColor('#1a356f')
    .setDescription(`
      **Modification du surnom :**

      Utilisateur : \`${member.user.tag}\`
      Ancien surnom : \`${oldNick}\`
      Nouveau surnom : \`${newNick}\``)
    .setThumbnail(member.user.displayAvatarURL({}))
  .setTimestamp();

  logs.send(embed)
}
