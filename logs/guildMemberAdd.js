const Discord = require('discord.js')
const ms = require('ms')

module.exports = async (client, member) => {
  member.guild.fetchInvites().then(async guildInvites => {

    let invites = client.invites
    const ei = invites[member.guild.id];
    client.invites[member.guild.id] = guildInvites;
    const invite = guildInvites.find(i => !ei.get(i.code) || ei.get(i.code).uses < i.uses) || null
    let inviter;
    if(!invite) {
      inviter = {
        tag: 'Aucun inviteur identifié.'
      }
    } else {
      inviter = client.users.cache.get(invite.inviter.id);
    }

    let logs = client.channels.cache.get(client.settings.logsChannelId);

    if (!logs) return;

    let embed = new Discord.MessageEmbed().setColor('GREEN').setDescription(`
      **Nouveau membre :**

      Pseudo de l'utilisateur : \`${member.user.username}\`
      Compte créé il y a : \`${ms(ms((Date.now()-member.user.createdTimestamp).toString()))}\`
      Invité par : \`${inviter.tag}\``).setThumbnail(member.user.displayAvatarURL({}))
    .setTimestamp();

    logs.send(embed)
  })
}
