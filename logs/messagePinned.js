const Discord = require('discord.js')

module.exports = async (client, message) => {
  let logs = client.channels.cache.get(client.settings.logsChannelId);
  if (!logs) return;

  let guildLogs = await message.guild.fetchAuditLogs()
  guildLogs = guildLogs.entries.first();

  let pinner;

  if (guildLogs.action !== 'MESSAGE_PIN') pinner = { tag: 'Inconnu' };
  if (!pinner) pinner = guildLogs.executor

  let embed = new Discord.MessageEmbed()
    .setColor('1a356f')
    .setDescription(`
      **Nouveau message epinglé :**

      Salon du message : <#${message.channel.id}>
      Message epinglé par : \`${pinner.tag}\`
      [Lien vers le message](${message.url})`)
    .setTimestamp()

    logs.send(embed)
}
