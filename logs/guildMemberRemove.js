const Discord = require('discord.js')
const ms = require('ms')

module.exports = async (client, member) => {
  let logs = client.channels.cache.get(client.settings.logsChannelId);

  if (!logs) return;

  let embed = new Discord.MessageEmbed().setColor('RED').setDescription(`
    **Un membre est partit :**

    Pseudo de l'utilisateur : \`${member.user.username}\`
    Compte créé il y a : \`${ms(ms((Date.now()-member.user.createdTimestamp).toString()))}\`
    Temps resté sur le serveur : \`${ms(ms((Date.now()-member.joinedTimestamp).toString()))}\``
  ).setThumbnail(member.user.displayAvatarURL({})).setTimestamp();

  logs.send(embed)
}
