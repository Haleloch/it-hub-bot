const db = require('quick.db')
const Discord = require('discord.js')

module.exports = async (client, message) => {
  if (!message.author) return;
  if (message.author.id == client.user.id && message.channel.id == client.settings.logsChannelId) {
    let channel = client.channels.cache.get(client.settings.logsChannelId);

    if (!channel) return;

    channel.send(message.embeds);

    return;
  }

  if (!message.content && !message.attachments.first()) return;

  let logs = await message.guild.fetchAuditLogs()

  logs = logs.entries.first();

  let deleter;

  if (logs.action !== 'MESSAGE_DELETE') deleter = message.author;
  if (!deleter) deleter = logs.executor;

  let embed = new Discord.MessageEmbed()
    .setColor('#1f6996')
    .setDescription(`**Suppression de message :**\n\nAuteur du message : ${message.author}\nMessage supprimé par : ${deleter}\nMessage supprimé dans : ${message.channel}\nContenu du message : ${message.content}`)
  .setTimestamp()

  if (message.attachments.first()) {
    let attachment = message.attachments.first()

    if (attachment.url.endsWith('.png') || attachment.url.endsWith('.jpg') || attachment.url.endsWith('.gif')) {
      embed.setImage(attachment.url)
      console.log('image set')
    }
  }

  let channel = client.channels.cache.get(client.settings.logsChannelId);

  if (!channel) return;

  channel.send(embed)
}
