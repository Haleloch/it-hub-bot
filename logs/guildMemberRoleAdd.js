const Discord = require('discord.js');

module.exports = async (client, member, role) => {
  let logs = client.channels.cache.get(client.settings.logsChannelId);

  if (!logs) return;

  let embed = new Discord.MessageEmbed()
    .setColor(role.hexColor)
    .setDescription(`
      **Rôle ajouté :**

      Membre ayant reçu le rôle : \`${member.user.tag}\`
      Rôle reçu : \`${role.name}\``)
    .setThumbnail(member.user.displayAvatarURL({}))
  .setTimestamp();

  logs.send(embed)
}
