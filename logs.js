const Discord = require("discord.js")
const ms = require("ms")

module.exports.guildMemberAdd = async (member, client) => {
  if(member.guild.id !== 708361087715770500) return;

  let avatar = member.user.avatarURL({ dynamic: true, size: 128})

  const channel = client.channels.cache.get("708395219606962236")
  if(!channel) return;

  let embed = new Discord.MessageEmbed()
    .setAuthor(`${member.user.username}#${member.user.discriminator}`, avatar)
    .setThumbnail(avatar)
    .setColor("GREEN")
    .setDescription(`:inbox_tray: ${member.user} a rejoint le serveur`)
    .addField("Création du compte", ms(ms(parseInt(Date.now() - member.user.createdAt))))
    .setFooter(`ID : ${member.user.id}`)
    .setTimestamp();

  channel.send(embed)
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
