const Discord = require("discord.js")
const db = require('quick.db')

module.exports = {
  name: "support",
  aliases: ["supp"],
  category: "support",
  run: async (client, msg, args) => {
    let alreadyOpen = msg.guild.channels.cache.find(ch => ch.topic == `<@${msg.author.id}> | Salon de support`)
    if(alreadyOpen) return msg.channel.send(':x: | Vous ne pouvez pas avoir plus d\'un salon de support ouvert à la fois !')
    let channel = await msg.guild.channels.create(msg.author.username, { parent: msg.guild.channels.cache.get(client.settings.supportCategoryId), topic: `<@${msg.author.id}> | `})

    msg.channel.send(`:white_check_mark: | Votre salon de support a été initialisé, veuillez continuer sa création dans <#${channel.id}> !`)

    if (!db.get(`supports`)) db.set(`supports`, {});

    db.set(`supports.${msg.author.id}`, channel.id)

    channel.send(msg.author, new Discord.MessageEmbed().setDescription('Création de votre salon de support en cours...\n\nVeuillez expliquer votre probleme :').setColor('ORANGE'))


    let filter = m => m.author.id = msg.author.id
    let collector = channel.createMessageCollector(filter, { time: 600000 });

    collector.on('collect', async m => {
      if(m.author.bot) return;
      m.channel.setTopic(`<@${m.author.id}> | Salon de support`)

      let issue = m.content

      let reactMenu = await m.channel.send(new Discord.MessageEmbed().setColor('ORANGE').setDescription('Votre problème, serait-il plutôt :\n\n💾 - Du software\n🖥️ - Du hardware\n🤷 - Vous ne savez pas\n\n**Si la réaction ne fonctionne pas, n\'hésitez pas a cliquer dessus plusieurs fois à la suite !**'))

      reactMenu.react('💾')
      reactMenu.react('🖥️')
      reactMenu.react('🤷')

      let emojis = ['🤷','💾','🖥️']

      filter2 = (reaction, user) => {
        return emojis.includes(reaction.emoji.name)
      }

      const menu = reactMenu.createReactionCollector(filter2, { time: 120000 })

      menu.on('collect', async (reaction, user) => {

        collector.stop()
        console.log(msg.author.id)
        console.log(user.id)
        if(user.id !== msg.author.id) return;
        if(user.bot) return;
        switch (reaction.emoji.name) {
          case '💾':
            m.channel.setName(`💾・${m.author.username}`)
          break;
          case '🖥️':
            m.channel.setName(`🖥️・${m.author.username}`)
          break;
          case '🤷':
            m.channel.setName(`🤷・${m.author.username}`)
          break;
          menu.stop()
        }

        m.channel.bulkDelete(99)
        let issueMsg = await m.channel.send(new Discord.MessageEmbed().setColor('GREEN').setDescription(`**Probleme de ${m.author} :**\n${issue}`));
        issueMsg.pin()
      })
    })
  }
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
