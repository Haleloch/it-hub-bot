const Discord = require("discord.js")
const db = require('quick.db')
const ms = require('ms')

module.exports = {
  name: "suggest",
  aliases: ["sug"],
  category: "utility",
  run: async (client, msg, args) => {
    let timestamp = Date.now() //maintenant
    let lastSuggest = db.get(`cooldown_${msg.guild.id}.suggest.${msg.author.id}`) || 0 //derniere suggestion
    let diff = Math.round(timestamp-lastSuggest) //

    if (diff < ms('6h')) {
      msg.delete()
      let toWait = Math.round(ms('6h')-diff)
      msg.channel.send(new Discord.MessageEmbed().setColor('RED').setDescription(`:x: | Vous ne pouvez pas utiliser cette commande actuellement. Veuillez réessayer dans \`${ms(ms(toWait.toString()))}\`.`))
      return;
    }

    let embed = new Discord.MessageEmbed()
      .setColor('BLUE')
      .setDescription(args.join(' '))
      .setAuthor(msg.author.username, msg.author.displayAvatarURL());

    let channel = client.channels.cache.get(client.settings.suggestionsChannelId);

    if (!channel) return msg.channel.send(new Discord.MessageEmbed().setColor('RED').setDescription(':x: | Le salon de suggestions n\'a pas été trouvé. Veuillez modifier l\'identifiant, puis réessayer.'))

    let message = await channel.send(embed);
    message.react('👍');
    message.react('👎');

    msg.channel.send(new Discord.MessageEmbed().setColor('GREEN').setDescription('Votre suggestion a bien ete envoyée ! Vous pourrez faire une nouvelle suggestion dans 6 heures.'));
    db.set(`cooldown_${msg.guild.id}.suggest.${msg.author.id}`, timestamp)
  }
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
