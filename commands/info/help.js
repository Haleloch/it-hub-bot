const { MessageEmbed } = require("discord.js");
const { stripIndents } = require("common-tags");
const { readdirSync } = require('fs')



module.exports = {
    name: "help",
    aliases: ["h"],
    category: "info",
    run: async (client, message, args,cmd,  lang) => {
        if (args[0]) {
            return getCMD(client, message, args[0]);
        } else {
            return getAll(client, message);
        }
    }
}

function getAll(client, message) {
    const embed = new MessageEmbed()
    const categories = readdirSync("./commands/")

    embed.setTitle(`𝗜𝗧 𝗛𝘂𝗯 - Commandes`)
    embed.setFooter(`🌐 ►𝗜𝗧 𝗛𝘂𝗯 | ${client.commands.size} Commandes`, client.user.displayAvatarURL);

    categories.forEach(category => {
        const dir = client.commands.filter(c => c.category === category)
        const capitalise = category.slice(0, 1).toUpperCase() + category.slice(1)
        let txt = (dir.map(c => `\`${c.name}\` · `).join(" "))
        txt = txt.substring(0, txt.length-2)
        console.log(txt)
        try {
            //category.toLowerCase() === "nom" ? "emoji" : ...
            embed.addField(`${category.toLowerCase() == 'info' ? ':information_source:' : category.toLowerCase() == 'moderation' ? '<:banhammer:785571602308792320>' : category.toLowerCase() == 'support' ? ':tools:' : category.toLowerCase() == 'utility' ? ':paperclips:' : ":question:"} ❯ ${capitalise} (${dir.size}):`,txt )
        } catch(e) {
            console.log(e)
        }
    })

    return message.channel.send(embed/*.setColor(client.invisible)*/)
}

function getCMD(client, message, input) {
    const embed = new MessageEmbed()

    const cmd = client.commands.get(input.toLowerCase()) || client.commands.get(client.aliases.get(input.toLowerCase()));

    let info = ''

    if (!cmd) {
        let info1 = `:x: Aucune commande **${input.toLowerCase()}** n'a été trouvée !`;
        return message.channel.send(embed.setColor("RED").setTitle(info1));
    }

    if (cmd.name)  embed.setTitle(`**🔤 Command name**: ${cmd.name}`)
    if (cmd.description) info += `\n**ℹ️ Description**: ${cmd.description}`
    if (cmd.aliases) info += `\n**🔀 Aliases**: ${cmd.aliases.map(a => `\`${a}\``).join(", ")}`;
    if (cmd.usage) {
        info += `\n**❓ Usage**: ${cmd.usage}`;
        embed.setFooter(`Syntax: <> = required, [] = optional`, message.guild.iconURL);
    }

    return message.channel.send(embed.setColor(client.invisible).setDescription(info));
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
