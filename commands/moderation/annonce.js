const Discord = require('discord.js')

module.exports = {
  name: "annonce",
  aliases: [],
  category: "moderation",
  run: async (client, message, args) => {

    async function makeEmbed(channel, embed, usr) {
      let field = {
        title: null,
        description: null
      }
      let message = await channel.send(new Discord.MessageEmbed().setColor('BLUE').setDescription('Que souhaitez vous faire ?\n\n:incoming_envelope: - Envoyer l\'annonce\n:pushpin: - Rajouter un paragraphe'));

      message.react('📨');
      message.react('📌');

      let filter = (reaction, user) => {
        return (reaction.emoji.name == '📌'  || reaction.emoji.name == '📨') && user.id == usr.id
      }

      let collector = message.createReactionCollector(filter, { time: 30000 });

      collector.on('collect', async (reaction, user) => {
        if (user.bot) return;
        switch(reaction.emoji.name) {
          case '📨':
            collector.stop()

            let mention = await channel.send(new Discord.MessageEmbed().setColor('BLUE').setDescription('Quelle mention voulez vous ?\n\n❌ - Sans mention\n✅ - Mention here\n❕ - Mention du role annonce\n❗ - Mention everyone\n'));

            mention.react('❌');
            mention.react('✅');
            mention.react('❕');
            mention.react('❗');

            let reactions = ['❌', '✅', '❕', '❗']

            let filter = (reaction, user) => {
              return true;
            }

            let mentionColl = mention.createReactionCollector(filter,  { time: 30000 });


            let chan = client.channels.cache.get(client.settings.announcementChannelId);

            if (!chan) return channel.send(new Discord.MessageEmbed().setColor('RED').setDescription(':x: | L\'id du salon d\'annonces est invalide.'));

            mentionColl.on('collect', async (reaction, user) => {
              if (user.bot) return;

              let annoucementMessage;
              switch (reaction.emoji.name) {
                case '❌':
                  annoucementMessage = await chan.send(embed)
                  annoucementMessage.react('<:valid:795737378127740935>');
                break;

                case '✅':
                  annoucementMessage = await chan.send("@here", embed);
                  annoucementMessage.react('<:valid:795737378127740935>');
                break;

                case '❕':
                  annoucementMessage = await chan.send(`<@&${client.settings.annoucementNotifRole}>`,embed);
                  annoucementMessage.react('<:valid:795737378127740935>');
                break;

                case '❗':
                  annoucementMessage = await chan.send("@everyone", embed);
                  annoucementMessage.react('<:valid:795737378127740935>');
                break
              }
            })
          break;

          case '📌':
            channel.send(new Discord.MessageEmbed().setColor('BLUE').setDescription('Quel est le titre de votre nouveau paragraphe ?'));

            let filter2 = (message) => {
              return message.author.id == usr.id
            };

            let coll = channel.createMessageCollector(filter2, { time: 120000 });

            let step = 0

            coll.on('collect', async msg => {
            step++;
            switch(step) {
              case 1:
                field.title = msg.content;
                msg.channel.send(new Discord.MessageEmbed().setColor('BLUE').setDescription('Veuillez maintenant entrer votre nouveau paragraphe :'));
              break;
              case 2:
                field.description = msg.content;
                embed.addField(field.title, field.description);
                makeEmbed(channel, embed, usr);
              break;
            }
            })
            collector.stop()
          break;
        }
      })
    }

    if (!message.member.roles.cache.has(client.settings.modRoleId)) return message.channel.send(new Discord.MessageEmbed().setColor('RED').setDescription(':x: | Vous ne pouvez pas executer cette commande !'));

    let embed = new Discord.MessageEmbed().setColor('#4e3f7b').setTimestamp();

    message.channel.send(new Discord.MessageEmbed().setColor('BLUE').setDescription('Quel est le titre de votre annonce ?'));

    let filter = (msg) => {
      return msg.author.id == message.author.id
    }

    let paragraph = {
      title: null,
      description: null
    }

    let title = message.channel.createMessageCollector(filter, { time: 30000 })

    title.once('collect', async newMessage => {
      embed.setAuthor(`${message.author.username} - ${newMessage.content}`, message.author.displayAvatarURL());

      message.channel.send(new Discord.MessageEmbed().setColor('BLUE').setDescription('Veuillez entrer le titre du premier paragraphe :'));

      let para1 = message.channel.createMessageCollector(filter, { time: 120000 });

      let step = 0

      para1.on('collect', async collected => {
        step++
        switch(step) {
          case 1:
            paragraph.title = collected.content
            message.channel.send(new Discord.MessageEmbed().setColor('BLUE').setDescription('Veuillez maintenant entrer le premier paragraphe :'))
          break;
          case 2:
          para1.stop()
            paragraph.description = collected.content;

            embed.addField(paragraph.title, paragraph.description)

            let embedToSend = await makeEmbed(message.channel, embed, message.author);

            /*

            let channel = client.channels.cache.get(client.settings.announcementChannelId);

            if (!channel) return message.channel.send(new Discord.MessageEmbed().setColor('RED').setDescription(':x: | L\'identifiant du salon annonce est invalide !'));

            channel.send(embedToSend)

            */
          break;
        }
      })
    })
  }
}
