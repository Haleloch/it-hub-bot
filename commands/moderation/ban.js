const Discord = require('discord.js')
const db = require('quick.db')
const ms = require('ms')

module.exports = {
  name: "ban",
  aliases: ['b'],
  category: "moderation",
  run: async (client, message, args) => {
    if (!message.member.roles.cache.has(client.settings.modRoleId)) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Vous n\'avez pas le role staff requis pour executer cela !'))

    let user = message.guild.member(message.mentions.users.first());
    if (!user) user = message.guild.members.cache.find(member => member.user.username == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.username.toLowerCase() == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.discriminator == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.tag == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.tag.toLowerCase() == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.id == args[0])

    if (!user) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Je n\'ai pas trouvé cet utilisateur. Veuillez réessayer avec le pseudo, le tag, l\'identifiant, ou une mention.'));

    user.ban({ days: 7, reason:`Banni par ${message.author.tag} | ${(args.join(' ') || 'Aucune raison fournie')}`})

    message.delete()
    message.channel.send(
      new Discord.MessageEmbed()
        .setColor('#1a356f')
        .setDescription(`:white_check_mark: | L'utilisateur \`${user.user.tag}\` a bien été banni !`)
    )

    let logs = client.channels.cache.get(client.settings.logsChannelId)
    if (!logs) return;

    logs.send(
      new Discord.MessageEmbed()
        .setColor('RED')
        .setDescription(`
          **Bannisement de membre :**

          Membre banni : \`${user.user.tag}\`
          Modérateur : \`${message.author.tag}\`
          Raison : \`${args.join(' ') || 'Aucune raison fournie'}\``)
    )
  }
}
