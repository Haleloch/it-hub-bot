const Discord = require('discord.js')
const db = require('quick.db')
const ms = require('ms')

module.exports = {
  name: "clear",
  aliases: ['cl'],
  category: "moderation",
  run: async (client, message, args) => {
    if (!message.member.roles.cache.has(client.settings.modRoleId)) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Vous n\'avez pas le role staff requis pour executer cela !'))

    let toDel = parseInt(args[0]);

    if (typeof toDel !== 'number' || !toDel || toDel > 100 || toDel < 1) {
      message.channel.send(
        new Discord.MessageEmbed()
          .setColor('#1a356f')
          .setDescription(':warning: | Votre nombre doit etre un entier compris entre \`1\` et \`100\`.')
      )

      return;
    }

    await message.delete()
    message.channel.bulkDelete(toDel);
    let confirm = await message.channel.send(
      new Discord.MessageEmbed()
        .setColor('#1a356f')
        .setDescription(`:white_check_mark: | J'ai bien supprimé \`${toDel.toString()}\` messages !`)
    )
    confirm.delete({timeout:5000});

    let logs = client.channels.cache.get(client.settings.logsChannelId);

    if (!logs) return;

    logs.send(
      new Discord.MessageEmbed()
        .setColor('RED')
        .setDescription(`
          **Suppression de message en masse :**

          Modérateur : \`${message.author.tag}\`
          Salon : ${message.channel}
          Nombre de messages supprimés : \`${toDel.toString()}\``)
    )
  }
}
