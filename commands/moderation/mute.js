const Discord = require('discord.js')
const db = require('quick.db')
const ms = require('ms')

module.exports = {
  name: "mute",
  aliases: ['m'],
  category: "moderation",
  run: async (client, message, args) => {
    if (!message.member.roles.cache.has(client.settings.modRoleId)) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Vous n\'avez pas le role staff requis pour executer cela !'))

    let user = message.guild.member(message.mentions.users.first());
    if (!user) user = message.guild.members.cache.find(member => member.user.username == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.username.toLowerCase() == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.discriminator == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.tag == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.tag.toLowerCase() == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.id == args[0])

    if (!user) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Je n\'ai pas trouvé cet utilisateur. Veuillez réessayer avec le pseudo, le tag, l\'identifiant, ou une mention.'));

    let muteRole = message.guild.roles.cache.get(client.settings.muteRole);

    if (!muteRole) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Le rôle de mute est invalide. Veuillez le vérifier dans le fichier settings.json'));

    if (user.roles.cache.has(muteRole.id)) return message.channel.send(new Discord.MessageEmbed().setColor('#1a256f').setDescription(':x: | Cet utilisateur est déjà muet !'))

    let logs = client.channels.cache.get(client.settings.logsChannelId);

    if (logs) {
      logs.send(new Discord.MessageEmbed().setColor('RED').setDescription(`
        **Mute de membre** :

        Utilisateur rendu muet : \`${user.user.username}\`
        Motif de la sanction : \`${args.slice(2).join(' ') || 'Aucun motif fourni'}\`
        Moderateur : \`${message.author.tag}\``))
    }

    user.roles.add(muteRole);
    message.delete()
    message.channel.send(
      new Discord.MessageEmbed()
        .setColor('#1a356f')
        .setDescription(`:white_check_mark: | L'utilisateur \`${user.user.tag}\` a bien ete rendu muet !`)
    )

    db.set(`tempmutes.${user.user.id}`, { id: user.user.id, timeout: 8640000000000000, guildId: message.guild.id });
  }
}
