const Discord = require('discord.js')
const db = require('quick.db')
const ms = require('ms')

module.exports = {
  name: "unmute",
  aliases: ['um'],
  category: "moderation",
  run: async (client, message, args) => {
    if (!message.member.roles.cache.has(client.settings.modRoleId)) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Vous n\'avez pas le rôle staff requis pour exécuter cela !'))

    let user = message.guild.member(message.mentions.users.first());
    if (!user) user = message.guild.members.cache.find(member => member.user.username == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.username.toLowerCase() == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.discriminator == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.tag == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.tag.toLowerCase() == args[0]);
    if (!user) user = message.guild.members.cache.find(member => member.user.id == args[0])

    if (!user) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Je n\'ai pas trouvé cet utilisateur. Veuillez réessayer avec le pseudo, le tag, l\'identifiant, ou une mention.'));

    let muteRole = message.guild.roles.cache.get(client.settings.muteRole);

    if (!muteRole) return message.channel.send(new Discord.MessageEmbed().setColor('#1a356f').setDescription(':x: | Le rôle de mute est invalide. Veuillez le vérifier dans le fichier settings.json'));

    if (!user.roles.cache.has(muteRole.id)) return message.channel.send(new Discord.MessageEmbed().setColor('#1a256f').setDescription(':x: | Cet utilisateur n\'est pas muet !'))

    let logs = client.channels.cache.get(client.settings.logsChannelId);

    if (logs) {
      logs.send(new Discord.MessageEmbed().setColor('RED').setDescription(`
        **Unmute de membre** :

        Utilisateur autorise a parler : \`${user.user.username}\`
        Moderateur : \`${message.author.tag}\``))
    }

    user.roles.remove(muteRole);
    message.delete()
    message.channel.send(
      new Discord.MessageEmbed()
        .setColor('#1a356f')
        .setDescription(`:white_check_mark: | L'utilisateur \`${user.user.tag}\` a bien été autorisé à parler !`)
    )

    db.set(`tempmutes.${user.user.id}`, { id: user.user.id, timeout: 0, guildId: message.guild.id });
  }
}
