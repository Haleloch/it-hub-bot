const Discord = require("discord.js")
const db = require("quick.db")
const moment = require("moment")
const ms = require('ms')

module.exports.welcomeImage = async (user, Canvas, Discord, guild) => {
    const canvas = Canvas.createCanvas(483, 171)

    Canvas.registerFont('./font.ttf', { family: "Custom" })

        const ctx = canvas.getContext("2d")

        const background = await Canvas.loadImage("./assets/welcome.png");

        ctx.drawImage(background, 0, 0, canvas.width, canvas.height)

        const avatarback = await Canvas.loadImage("https://s2.qwant.com/thumbr/0x0/5/4/6254422b3bf869c95fa1644497814a788af7b7b851fbbbed5e9f8f6301066f/circle-xxl.png?u=https%3A%2F%2Fwww.iconsdb.com%2Ficons%2Fpreview%2Fwhite%2Fcircle-xxl.png&q=0&b=1&p=0&a=1")

        ctx.drawImage(avatarback, 50, 25, 120, 120)

        var usrnm = user.username

        if(usrnm.length > 12) {
            usrnm = usrnm.split("")
            usrnm.length = 11
            usrnm = usrnm.join("")
            usrnm += "..."
        }

        ctx.font = 'bold 22px Custom';
        ctx.fillStyle = '#000';
        ctx.fillText(`${usrnm}#${user.discriminator}`, canvas.width / 2.5, 50);

        //Option 1 :
        /*
        ctx.font = 'bold 30px Custom';
        ctx.fillText("Bienvenue !", canvas.width / 2.5, 75);

        ctx.font = 'bold 18px Custom';
        ctx.fillText(`Tu es le ${guild.members.cache.size}è\n membre !`, canvas.width / 2.5, 100)
        */

        //Option 2 :
        /*
        ctx.font = 'bold 34px Custom';
        ctx.fillText("Bienvenue !", canvas .width/2.5, 80);

        ctx.font = 'bold 16px Custom';
        ctx.fillText(`Tu es le ${guild.members.cache.size}è membre !`, canvas.width / 2.5, 110)
        */

        //Option 3 :
        ctx.font = 'bold 34px Custom';
        ctx.fillStyle = "#008DAD"
        ctx.fillText("Bienvenue !", canvas .width/2.5, 100);

        ctx.font = 'bold 15px Custom';
        ctx.fillStyle = "#000"
        ctx.fillText(`Tu es le ${guild.members.cache.size}ème membre !`, canvas.width / 2.5, 130)

        ctx.beginPath();

        ctx.arc(110, canvas.height/2, 50, 0, Math.PI*2, true);

        ctx.closePath();

        ctx.clip();

        const avatar = await Canvas.loadImage(user.displayAvatarURL({ format: 'png' }))

        ctx.drawImage(avatar, 60, 35, 100, 100)

        return new Discord.MessageAttachment(canvas.toBuffer(), "wlcm.png");
}

module.exports.checkSupports = async (client) => {
  for (i in db.get(`supports`)) {
    let channelId = db.get(`supports.${i}`);

    if (!channelId) return;

    let channel = client.channels.cache.get(channelId);

    if (!channel) return;

    let lastMsg = channel.lastMessage;
    if (!lastMsg) return;
    let timestamp = lastMsg.createdTimestamp;

    let maxTimestamp = ms('4d');

    if ((Date.now()-timestamp)>=maxTimestamp) {
      let user = client.users.cache.get(i);

      user.send(new Discord.MessageEmbed().setColor('#4e3f7b').setDescription(':warning: | Votre salon de support a été automatiquement supprimé, pour une inactivité supérieure à 4 jours. Vous pouvez tout de même en recréer un a l\'aide de la commande `!support`.'))

      channel.delete()
    }
  }
}

module.exports.dbMemberAdd = async (member, invites, db, client) => {

  member.guild.fetchInvites().then(async guildInvites => {

    const ei = invites[member.guild.id];
    invites[member.guild.id] = guildInvites;
    const invite = guildInvites.find(i => !ei.get(i.code) || ei.get(i.code).uses < i.uses) || null
    let inviter;
    if(!invite) {
      inviter = client.users.cache.get(member.user.id);
    } else {
      inviter = client.users.cache.get(invite.inviter.id);
    }

    if((Date.now()-member.user.createdAt) < 345600000) {
      return db.set(`invites_${member.guild.id}.${inviter.id}.fake`, (db.get(`invites_${member.guild.id}.${inviter.id}.fake`) || 0) +1)
    }
    db.set(`invites_${member.guild.id}.${inviter.id}.true`, (db.get(`invites_${member.guild.id}.${inviter.id}.true`) || 0) +1)

    if (!db.get(`inviter`)) db.set(`inviter`, {});

    db.set(`inviter.${member.user.id}`, inviter.id)

    if(client.settings.invites[Math.round((db.get(`invites_${member.guild.id}.${inviter.id}.true`) || 0) + (db.get(`invites_${member.guild.id}.${inviter.id}.bonus`) || 0))]) {
      console.log(db.get(`invites_${member.guild.id}.${inviter.id}.true`) || 0 + db.get(`invites_${member.guild.id}.${inviter.id}.bonus`) || 0)
      if(member.guild.member(inviter).roles.cache.has(member.guild.roles.cache.get("708697095141589042"))) return;
      if(inviter.bot) return;
      client.functions.addRole(member.guild.member(inviter), member.guild.roles.cache.get(client.settings.invites[Math.round((db.get(`invites_${member.guild.id}.${inviter.id}.true`) || 0) + (db.get(`invites_${member.guild.id}.${inviter.id}.bonus`) || 0))]))
    }
  })
}

module.exports.addRole = async (member, role) => {
  member.roles.add(role)
}

module.exports.postStats = async (client, guildId) => {
    //if(!db.get(`stats_${guildId}.chanid`)) return;

    const stats = db.get(`stats_${guildId}`)

    const { CanvasRenderService } = require("chartjs-node-canvas");
    const width = 800;
    const height = 300;
    // White color and bold font
    const ticksOptions = [{ ticks: { fontColor: "white", fontStyle: "bold" } }];
    const options = {
        // Hide legend
        legend: { display: false },
        scales: { yAxes: ticksOptions, xAxes: ticksOptions }
    };

    let h00 = db.get(`stats_${guildId}.hourMessages.0`) || 0
    let h1 = db.get(`stats_${guildId}.hourMessages.1`) || 0
    let h2 = db.get(`stats_${guildId}.hourMessages.2`) || 0
    let h3 = db.get(`stats_${guildId}.hourMessages.3`) || 0
    let h4 = db.get(`stats_${guildId}.hourMessages.4`) || 0
    let h5 = db.get(`stats_${guildId}.hourMessages.5`) || 0
    let h6 = db.get(`stats_${guildId}.hourMessages.6`) || 0
    let h7 = db.get(`stats_${guildId}.hourMessages.7`) || 0
    let h8 = db.get(`stats_${guildId}.hourMessages.8`) || 0
    let h9 = db.get(`stats_${guildId}.hourMessages.9`) || 0
    let h10 = db.get(`stats_${guildId}.hourMessages.10`) || 0
    let h11 = db.get(`stats_${guildId}.hourMessages.11`) || 0
    let h12 = db.get(`stats_${guildId}.hourMessages.12`) || 0
    let h13 = db.get(`stats_${guildId}.hourMessages.13`) || 0
    let h14 = db.get(`stats_${guildId}.hourMessages.14`) || 0
    let h15 = db.get(`stats_${guildId}.hourMessages.15`) || 0
    let h16 = db.get(`stats_${guildId}.hourMessages.16`) || 0
    let h17 = db.get(`stats_${guildId}.hourMessages.17`) || 0
    let h18 = db.get(`stats_${guildId}.hourMessages.18`) || 0
    let h19 = db.get(`stats_${guildId}.hourMessages.19`) || 0
    let h20 = db.get(`stats_${guildId}.hourMessages.20`) || 0
    let h21 = db.get(`stats_${guildId}.hourMessages.21`) || 0
    let h22 = db.get(`stats_${guildId}.hourMessages.22`) || 0
    let h23 = db.get(`stats_${guildId}.hourMessages.23`) || 0

    const canvasRenderService = new CanvasRenderService(width, height, () => {});
    const image = await canvasRenderService.renderToBuffer({
        type: "line",
        data: {
            labels: ['00h','1h','2h','3h','4h','5h','6h','7h','8h','9h','10h','11h','12h','13h','14h','15h','16h','17h','18h','19','20h','21h','22h','23h'],
            datasets: [
                {
                    label: `Evolution du nombre de messages dans le serveur a chaque heure`,
                    data: [h00,h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12,h13,h14,h15,h16,h17,h18,h19,h20,h21,h22,h23],
                    // The color of the line (the same as the fill color with full opacity)
                    //borderColor: "rgb(61,148,192)",
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    // The color of the line (the same as the fill color with full opacity)
                    borderColor: "rgb(61,148,192)",
                    // Fill the line with color
                    fill: true,
                    // Blue color and low opacity
                    backgroundColor: "rgba(61,148,192,0.1)"
                }
            ]
        },
        options
    });

    let atta = new Discord.MessageAttachment(image, "stats.png")

    //db.set(`stats_${guildId}`, {})

    let channelsLb = [];

    for (const id in db.get(`stats_${guildId}.channels`)) {
      channelsLb.push({
        id:id,
        msgs:db.get(`stats_${guildId}.channels[${id}]`)
      })
    }

    var lb = sortByKey(channelsLb, "msgs")

    let topIds = []

    if(lb.length > 5) lb.length = 5

    for (const channel in lb) {
      topIds.push(`<#${channelsLb[channel].id}> (${channelsLb[channel].msgs})`)
    }

    let usersLb = []

    for (const id in db.get(`stats_${guildId}.users.ids`)) {
      usersLb.push({
        id:id,
        msgs:db.get(`stats_${guildId}.users.ids[${id}].count`)
      })
    }

    var usrlb = sortByKey(usersLb, 'msgs')

    if (usersLb.length > 5) usersLb.length = 5

    let topUserIds = [];

    for (const user in usrlb) {
        topUserIds.push(`<@${usersLb[user].id}> (${usersLb[user].msgs})`)
    }

    let embed = new Discord.MessageEmbed()
      .addField(`Statistiques des dernieres 24 heures :`, `\n📥 | Joins : ${stats.joins || 0}\n📤 | Leaves : ${stats.leaves || 0}\n💬 | Messages : ${stats.messages || 0}\n👥 | Messages moyens par membres : ${Math.round(stats.messages/client.guilds.cache.get(guildId).memberCount)}\n📟 |Nombre de membres actifs : ${stats.users.count}\n✉️ | Nombre moyen de message envoye par membre actif : ${Math.round(stats.messages/stats.users.count)}\n\nClassement des salons par messages :\n${topIds.join(`\n`)}\n\nEvolution du nombre de messages envoyes par heure :`)
      .attachFiles(atta)
      .setImage("attachment://stats.png")

    return embed;
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        let x = a[key]; let y = b[key];
        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
    });
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
